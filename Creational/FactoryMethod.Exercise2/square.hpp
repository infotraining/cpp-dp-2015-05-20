#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "shape.hpp"

// TODO: Dodać klase Square
namespace Drawing
{
    class Square : public ShapeBase
    {
        int size_;
    public:
        Square(int x = 0, int y  = 0, int size = 0) : ShapeBase(x, y), size_{size}
        {}

        void draw() const
        {
            std::cout << "Drawing a square at " << point() << " with size " << size_ << std::endl;
        }

        void read(std::istream &in)
        {
            Point pt;
            int size;

            in >> pt >> size;

            set_point(pt);
            set_size(size);
        }

        void write(std::ostream &out)
        {
            out << "Square " << point() << " " << size() << std::endl;
        }

        int size() const;

        void set_size(int size);
    };

    int Square::size() const
    {
        return size_;
    }

    void Square::set_size(int size)
    {
        size_ = size;
    }

}


#endif /* SQUARE_HPP_ */
