#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
    cout << "Start of main..." << endl;

    Meyers::Singleton::instance().do_something();

    auto& singleObject = Meyers::Singleton::instance();
	singleObject.do_something();
}
