#include <vector>
#include <memory>
#include <unordered_map>
#include <typeindex>
#include <functional>

#include "employee.hpp"
#include "hrinfo.hpp"

//HRInfo* gen_info(const Employee& e)
//{
//	if (const Salary* s = dynamic_cast<const Salary*>(&e))
//		return new StdInfo(s);
//	if (const Hourly* h = dynamic_cast<const Hourly*>(&e))
//		return new StdInfo(h);
//	if (const Temp* t = dynamic_cast<const Temp*>(&e))
//		return new TempInfo(t);

//    throw std::runtime_error("Wrong employee type");
//}

using HRInfoPtr = std::unique_ptr<HRInfo>;
using HRInfoCreator = std::function<HRInfoPtr (const Employee&)>;

class HRInfoFactory
{
    std::unordered_map<std::type_index, HRInfoCreator> creators_;
public:
    bool register_creator(const std::type_info& type_id, HRInfoCreator creator)
    {
        return creators_.insert(std::make_pair(std::type_index(type_id), creator)).second;
    }

    HRInfoPtr create(const Employee& emp)
    {
        return creators_.at(std::type_index(typeid(emp)))(emp);
    }
};

int main()
{
	using namespace std;

    HRInfoFactory factory_hrinfo;

    factory_hrinfo.register_creator(typeid(Salary),
                                    [] (const Employee& emp) { return std::unique_ptr<HRInfo>(new StdInfo(&emp)); });
    factory_hrinfo.register_creator(typeid(Hourly),
                                    [] (const Employee& emp) { return std::unique_ptr<HRInfo>(new StdInfo(&emp)); });
    factory_hrinfo.register_creator(typeid(Temp),
                                    [] (const Employee& emp) { return std::unique_ptr<HRInfo>(new TempInfo(&emp)); });



    vector<std::unique_ptr<Employee>> emps;
    emps.emplace_back(new Salary("Jan Kowalski"));
    emps.emplace_back(new Hourly("Adam Nowak"));
    emps.emplace_back(new Temp("Anna Nowakowska"));

	cout << "HR Report:\n---------------\n";


	// generowanie obiektów typu HRInfo
    for(const auto& emp : emps)
	{
        auto hri = factory_hrinfo.create(*emp);
		hri->info();
		cout << endl;
	} // wyciek pamięci
}
