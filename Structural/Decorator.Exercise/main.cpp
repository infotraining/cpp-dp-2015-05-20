#include <memory>
#include <cassert>
#include "coffeehell.hpp"

void client(std::unique_ptr<Coffee> coffee)
{
    std::cout << "Description: " << coffee->get_description() << "; Price: " << coffee->get_total_price() << std::endl;
    coffee->prepare();
}


template <typename HeadDecorator, typename... TailDecorators>
struct DecorateWith
{
    static std::unique_ptr<Coffee> make_decorator(std::unique_ptr<Coffee> component)
    {
        auto decorator_component = std::make_unique<HeadDecorator>(move(component));

        return DecorateWith<TailDecorators...>::make_decorator(move(decorator_component));
    }
};

template <typename HeadDecorator>
struct DecorateWith<HeadDecorator>
{
    static std::unique_ptr<Coffee> make_decorator(std::unique_ptr<Coffee> component)
    {
        return std::make_unique<HeadDecorator>(move(component));
    }
};


class CoffeeBuilder
{
    std::unique_ptr<Coffee> coffee_;
public:
    template <typename BaseCoffee>
    CoffeeBuilder& create_base()
    {
        coffee_.reset(new BaseCoffee);

        return *this;
    }

    template <typename... Condiments>
    CoffeeBuilder& add()
    {
        assert(coffee_);

        coffee_ = DecorateWith<Condiments...>::make_decorator(move(coffee_));

        return *this;
    }

    std::unique_ptr<Coffee> get_coffee()
    {
        return move(coffee_);
    }
};

int main()
{
    using namespace std;

    auto cf = make_unique<Whipped>(
                make_unique<Whisky>(
                    make_unique<Whisky>(
                        make_unique<ExtraEspresso>(
                            make_unique<Espresso>()))));
    client(move(cf));

    std::cout << "\n\n";

    CoffeeBuilder cb;

    cb.create_base<Espresso>();
    cb.add<ExtraEspresso, Whisky, Whisky>().add<Whipped>();

    client(cb.get_coffee());
}
