#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <string>
#include <iostream>
#include <boost/signals2.hpp>


// Subject
class Stock
{
private:
	std::string symbol_;
	double price_;

    boost::signals2::signal<void (const std::string&, double)> price_changed_;

public:
	Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
	{

	}

	std::string get_symbol() const
	{
		return symbol_;
	}

	double get_price() const
	{
		return price_;
	}

	// TODO: rejestracja obserwatora
    boost::signals2::connection connect(std::function<void (const std::string&, double)> slot)
    {
        return price_changed_.connect(slot);
    }

	void set_price(double price)
	{
        if (price != price_)
        {

            price_ = price;

            // TODO: powiadomienie inwestorow o zmianie kursu...
            price_changed_(symbol_, price_);
        }
	}
};




class Investor
{
	std::string name_;
public:
	Investor(const std::string& name) : name_(name)
	{
	}

    void update_portfolio(const std::string& symbol, double price)
	{
        std::cout << name_ << " notified: " << symbol << " - " << price << std::endl;
	}
};

#endif /*STOCK_HPP_*/
