#ifndef STATE_HPP_
#define STATE_HPP_

#include <iostream>
#include <string>
#include <memory>
#include <typeinfo>


class Context;

// "State"
class State
{
public:
    virtual std::unique_ptr<State> handle(Context& context) = 0;
	virtual ~State() {}
};

// "ConcreteStateA"
class ConcreteStateA : public State
{
public:
    std::unique_ptr<State> handle(Context& context);
};


// "ConcreteStateB"
class ConcreteStateB : public State
{
public:
    std::unique_ptr<State> handle(Context& context);
};


// "Context"
class Context
{
	std::unique_ptr<State> state_;

public:
	Context(std::unique_ptr<State> initial_state) : state_{std::move(initial_state)}
	{
	}

	Context(const Context&) = delete;
	Context& operator=(const Context&) = delete;

	void request()
	{
        state_ = state_->handle(*this);
	}
};


std::unique_ptr<State> ConcreteStateA::handle(Context& context)
{
	std::cout << "Context works in ConcreteStateA" << std::endl;

    return std::unique_ptr<State>{ new ConcreteStateB() };
}

std::unique_ptr<State> ConcreteStateB::handle(Context& context)
{
	std::cout << "Context works in ConcreteStateB" << std::endl;

    return std::unique_ptr<State>{ new ConcreteStateA() };
}

#endif /*STATE_HPP_*/
