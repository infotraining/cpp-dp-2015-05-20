#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>
#include <memory>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

using Results = std::vector<StatResult>;

enum StatisticsType
{
	AVG, MINMAX, SUM
};

class Statistics
{
public:
    virtual void calculate(const std::vector<double>& data, Results& results) = 0;
    virtual ~Statistics() = default;
};

class Avg : public Statistics
{
    // Statistics interface
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        StatResult result("AVG", avg);
        results.push_back(result);
    }
};

class Min : public Statistics
{
    // Statistics interface
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double min = *(std::min_element(data.begin(), data.end()));

        results.push_back(StatResult("MIN", min));
    }
};

class Max : public Statistics
{
    // Statistics interface
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double max = *(std::max_element(data.begin(), data.end()));

        results.push_back(StatResult("MAX", max));
    }
};

class Sum : public Statistics
{
    // Statistics interface
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        results.push_back(StatResult("SUM", sum));
    }
};

using StatisticsPtr = std::shared_ptr<Statistics>;

class StatGroup : public Statistics
{
    std::vector<StatisticsPtr> stats_;

    // Statistics interface
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        for(const auto& stat : stats_)
            stat->calculate(data, results);
    }

    void add(StatisticsPtr statistics)
    {
        stats_.push_back(statistics);
    }
};

class DataAnalyzer
{
    StatisticsPtr statistics_;
	std::vector<double> data_;
	Results results_;
public:
    DataAnalyzer(StatisticsPtr stat) : statistics_(stat)
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();
		results_.clear();

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data_.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";
	}

    void set_statistics(StatisticsPtr statistics)
	{
        statistics_ = statistics;
	}

	void calculate()
	{
        statistics_->calculate(data_, results_);
	}

	const Results& results() const
	{
		return results_;
	}
};

void show_results(const Results& results)
{
    for(const auto& rslt : results)
		std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
    auto AVG = std::make_shared<Avg>();
    auto MIN = std::make_shared<Min>();
    auto MAX = std::make_shared<Max>();
    auto MINMAX = std::make_shared<StatGroup>();
    MINMAX->add(MIN);
    MINMAX->add(MAX);
    auto SUM = std::make_shared<Sum>();

    auto STD_STATS = std::make_shared<StatGroup>();
    STD_STATS->add(AVG);
    STD_STATS->add(MINMAX);
    STD_STATS->add(SUM);

    DataAnalyzer da {STD_STATS};
	da.load_data("data.dat");
	da.calculate();

    show_results(da.results());

	std::cout << "\n\n";

	da.load_data("new_data.dat");
	da.calculate();

	show_results(da.results());
}
